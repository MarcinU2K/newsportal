package com.newsportal.repository.user;

import org.springframework.data.repository.CrudRepository;

import com.newsportal.domain.user.User;

public interface UserRepository extends CrudRepository<User, String>{

	User findByUsername(String username);

	void save(org.springframework.security.core.userdetails.User user);

}
