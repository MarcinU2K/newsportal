package com.newsportal.configuration;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.newsportal.domain.user.Group;
import com.newsportal.domain.user.Role;
import com.newsportal.domain.user.User;
import com.newsportal.repository.user.UserRepository;

@Service
public class MongoUserDetailService implements UserDetailsService{
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		log.error("in loadUserByUsername");
		
		User user = userRepository.findByUsername(username);
		
		boolean enabled = true;
		boolean accountNonExpired = true;
		boolean credentialsNonExpired = true;
		boolean accountNonLocked = true;
		
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), 
				enabled, accountNonExpired, 
				credentialsNonExpired, accountNonLocked, 
				getAuthorities(user.getGroups()));
	}
	
	private List<SimpleGrantedAuthority> getAuthorities(List<String> groups) {
		List<SimpleGrantedAuthority> authList = new ArrayList<>();
		
//		for(Role role: roles) {
//			authList.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
//			log.error(role.getName());
//		}
		
		for(String group : groups) {
			log.error("group is: " + group);
			authList.add(new SimpleGrantedAuthority(group));
		}
		
		return authList;
	}

}
