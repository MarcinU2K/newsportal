package com.newsportal.domain.user;

import java.util.List;

public class Group {
	
	private String groupName;
	
	private List<String> authorities;
	
	public Group() {
		super();
	}

	public Group(String groupName, List<String> authorities) {
		super();
		this.groupName = groupName;
		this.authorities = authorities;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}
}
