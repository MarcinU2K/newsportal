package com.newsportal.domain;

public enum Section {
	NEWS, SPORT, BUSINESS, TECH, MOTO, KNOWLEDGE, ENTERTAINMENT, STYLE, UNASSIGNED
}
