package com.newsportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.newsportal.domain.user.User;
import com.newsportal.service.user.UserService;

@RestController
public class MainController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/user/register" , method = RequestMethod.POST)
	public User register(@RequestBody User user) {
		return userService.register(user);	
	}
	
	@RequestMapping(value = "/user/remove/{id}/" , method = RequestMethod.DELETE)
	public String removeUser(@PathVariable("id") String id) {
		return userService.removeUser(id);
	}

	@RequestMapping(value = "/id/{id}/addrole/{role}/", method = RequestMethod.PUT)
	public User addNewRole(@PathVariable("id")String id, @PathVariable("role") String role) {
		return userService.addNewRole(id, role);
	}
	
	@RequestMapping(value = "/id/{id}/removerole/{role}/", method = RequestMethod.PUT)
	public User removeRole(@PathVariable("id")String id, @PathVariable("role") String role) {
		return userService.removeRole(id, role);
	}
}
