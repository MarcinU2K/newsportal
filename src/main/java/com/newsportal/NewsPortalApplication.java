package com.newsportal;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.servlet.HandlerExceptionResolver;

import com.newsportal.domain.user.Role;
import com.newsportal.domain.user.User;
import com.newsportal.repository.user.UserRepository;

@SpringBootApplication
@Configuration
public class NewsPortalApplication {
	
	private final Logger log = LoggerFactory.getLogger(getClass());

	public static void main(String[] args) {
		SpringApplication.run(NewsPortalApplication.class, args);
	}
	
	@Bean
	public HandlerExceptionResolver sentryExceptionResolver() {
	    return new io.sentry.spring.SentryExceptionResolver();
	}
	
	@Bean
	public ServletContextInitializer sentryServletContextInitializer() {
	    return new io.sentry.spring.SentryServletContextInitializer();
	}
	
	@Autowired
	public void authenticationManager(AuthenticationManagerBuilder builder, UserRepository userRepository) throws Exception {
		builder.userDetailsService(new UserDetailsService() {
			
			@Override
			public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
				
				log.error("in loadUserByUsername");
				
				User user = userRepository.findByUsername(username);
				
				boolean enabled = true;
				boolean accountNonExpired = true;
				boolean credentialsNonExpired = true;
				boolean accountNonLocked = true;
				
				return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), 
						enabled, accountNonExpired, 
						credentialsNonExpired, accountNonLocked, 
						getAuthorities(user.getRoles()));
			}
			
			private List<SimpleGrantedAuthority> getAuthorities(List<Role> roles) {
				List<SimpleGrantedAuthority> authList = new ArrayList<>();
				
				for(Role role: roles) {
					authList.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
					log.error(role.getName());
				}
				
				return authList;
			}
		});
	}
}
